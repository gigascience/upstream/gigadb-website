[DOI]
10.5524/100925

[Title]
Supporting data for "Chromosome-level genome assemblies of <i>C. argus</i> and <i>C. maculata</i> and comparative analysis of their temperature adaptability"

[Release Date]
2021-09-01

[Citation]
Ou M; Huang R; Yang C; Gui B; Luo Q; Zhao J; Li Y; Liao L; Zhu Z; Wang Y; Chen K (2021): Supporting data for "Chromosome-level genome assemblies of <i>C. argus</i> and <i>C. maculata</i> and comparative analysis of their temperature adaptability" GigaScience Database. https://dx.doi.org/10.5524/100925

[Dataset Type]


[Dataset Summary]
<i>Channa argus</i> and <i>Channa maculata</i> are the main cultured species of the family Channidae. The relationship between them is close enough that they can mate, however their temperature adaptability is quite different. <br>In this study, we sequenced and assembled the whole genomes of <i>C. argus</i> and <i>C. maculata</i> for the first time and obtained chromosome-level genome assemblies of 630.39 and 618.82 Mb, respectively. Contig N50 was 13.20 and 21.73 Mb, scaffold N50 was 27.66 and 28.37 Mb, with 28,054 and 24,115 coding genes annotated for <i>C. argus</i> and <i>C. maculata</i>, respectively. <i>C. argus</i> and <i>C. maculata</i> have 24 and 21 chromosomes, respectively. Three pairs of chromosomes in <i>C. argus</i> correspond to three chromosomes in <i>C. maculata</i>, suggesting three chromosomal fusion events in <i>C. maculata</i>. Comparative analysis of their gene families showed that some immune-related genes were unique or expandable to <i>C. maculata</i>, such as genes related to herpes simplex infection. The transcriptome differences related to temperature adaptation revealed that the brain and liver of <i>C. argus</i> rapidly produced more DEGs than <i>C. maculata</i>. The genes in the FoxO signalling pathway were significantly enriched in <i>C. argus</i> during the cooling process (P < 0.05), and the expression of three transcription factor genes in this pathway was significantly different between <i>C. argus</i> and <i>C. maculata</i> (P< 0.01). <br><i>C. maculata</i> may have higher resistance to certain diseases, while <i>C. argus</i> has a faster and stronger response to low-temperature stress, and thus has better adaptability to a low-temperature environment. This study provides a high-quality genome research platform for follow-up studies of Channidae, and provides important clues for the differences in the low-temperature adaptation of fish.

[File name] - [File Description] - [File Location]
readme_100925.txt -  - https://s3.ap-northeast-1.wasabisys.com/gigadb-datasets/dev/pub/10.5524/100001_101000/100925/readme_100925.txt
Additional_File_10.csv - Tabular data with details of status and mortality of C. argus and C. maculata during cooling - https://s3.ap-northeast-1.wasabisys.com/gigadb-datasets/live/pub/10.5524/100001_101000/100925/Additional_files/Additional_File_10.csv
Additional_File_11.csv - Tabular data with statistics of transcriptome sequencing. - https://s3.ap-northeast-1.wasabisys.com/gigadb-datasets/live/pub/10.5524/100001_101000/100925/Additional_files/Additional_File_11.csv
Additional_File_14.csv - Tabular data with details of 10 transcription factor genes in the FoxO signalling pathway of C. argus and C. maculata. - https://s3.ap-northeast-1.wasabisys.com/gigadb-datasets/live/pub/10.5524/100001_101000/100925/Additional_files/Additional_File_14.csv
Additional_File_2.csv - Tabular data with details of mapping rates of the Illumina sequencing data - https://s3.ap-northeast-1.wasabisys.com/gigadb-datasets/live/pub/10.5524/100001_101000/100925/Additional_files/Additional_File_2.csv
Additional_File_3.csv - Tabular data with details of integrity of 3,354 conserved core genes - https://s3.ap-northeast-1.wasabisys.com/gigadb-datasets/live/pub/10.5524/100001_101000/100925/Additional_files/Additional_File_3.csv
Additional_File_4.csv - Tabular data with details of annotation of repetitive sequences. - https://s3.ap-northeast-1.wasabisys.com/gigadb-datasets/live/pub/10.5524/100001_101000/100925/Additional_files/Additional_File_4.csv
Additional_File_5.csv - Tabular data with details of annotation of coding genes - https://s3.ap-northeast-1.wasabisys.com/gigadb-datasets/live/pub/10.5524/100001_101000/100925/Additional_files/Additional_File_5.csv
Additional_File_6.csv - Tabular data with details of annotation of non-coding RNA. - https://s3.ap-northeast-1.wasabisys.com/gigadb-datasets/live/pub/10.5524/100001_101000/100925/Additional_files/Additional_File_6.csv
Additional_File_7_All_families.csv - Tabular data with details of orthogroup annotation for fish species referred to in this study. - https://s3.ap-northeast-1.wasabisys.com/gigadb-datasets/live/pub/10.5524/100001_101000/100925/Additional_files/Additional_File_7_All_families.csv
Additional_File_7_C_argus_specific.csv - Tabular data with details of specific genes in C. argus - https://s3.ap-northeast-1.wasabisys.com/gigadb-datasets/live/pub/10.5524/100001_101000/100925/Additional_files/Additional_File_7_C_argus_specific.csv
Additional_File_7_C_maculata_specific.csv - Tabular data with details of specific genes in C. maculata - https://s3.ap-northeast-1.wasabisys.com/gigadb-datasets/live/pub/10.5524/100001_101000/100925/Additional_files/Additional_File_7_C_maculata_specific.csv
Additional_File_7_Single-copy_gene_families.csv - Tabular data with list of single-copy genes - https://s3.ap-northeast-1.wasabisys.com/gigadb-datasets/live/pub/10.5524/100001_101000/100925/Additional_files/Additional_File_7_Single-copy_gene_families.csv
Additional_File_8_C_argus_contraction.csv - Tabular data with details of gene family statistics for contraction for C. argus - https://s3.ap-northeast-1.wasabisys.com/gigadb-datasets/live/pub/10.5524/100001_101000/100925/Additional_files/Additional_File_8_C_argus_contraction.csv
Additional_File_8_C_argus_expansions.csv - Tabular data with details of gene family statistics for expansion for C. argus - https://s3.ap-northeast-1.wasabisys.com/gigadb-datasets/live/pub/10.5524/100001_101000/100925/Additional_files/Additional_File_8_C_argus_expansions.csv
Additional_File_8_C_maculata_contraction.csv - Tabular data with details of gene family statistics for contraction for C. maculata - https://s3.ap-northeast-1.wasabisys.com/gigadb-datasets/live/pub/10.5524/100001_101000/100925/Additional_files/Additional_File_8_C_maculata_contraction.csv
Additional_File_8_C_maculata_expansions.csv - Tabular data with details of gene family statistics for expansion for C. maculata - https://s3.ap-northeast-1.wasabisys.com/gigadb-datasets/live/pub/10.5524/100001_101000/100925/Additional_files/Additional_File_8_C_maculata_expansions.csv
Fig.1.tif - Figure showing genome assembly and evolutionary analysis of C. argus and C. maculata. - https://s3.ap-northeast-1.wasabisys.com/gigadb-datasets/live/pub/10.5524/100001_101000/100925/Additional_files/Fig.1.tif
Fig.2.tif - Figure showing comparative analysis of the C. argus and the C. maculata genomes. - https://s3.ap-northeast-1.wasabisys.com/gigadb-datasets/live/pub/10.5524/100001_101000/100925/Additional_files/Fig.2.tif
Fig.3.tif - Figure showing verification of chromosome structure difference between C. argus and C. maculata genomes. - https://s3.ap-northeast-1.wasabisys.com/gigadb-datasets/live/pub/10.5524/100001_101000/100925/Additional_files/Fig.3.tif
Fig.4.tif - Figure showing low temperature experiment and transcriptome sequencing of C. argus and C. maculata. - https://s3.ap-northeast-1.wasabisys.com/gigadb-datasets/live/pub/10.5524/100001_101000/100925/Additional_files/Fig.4.tif
Fig.5.tif - Figure showing number of DEGs in brain and liver of C. argus (A) and C. maculata (B) during cooling. - https://s3.ap-northeast-1.wasabisys.com/gigadb-datasets/live/pub/10.5524/100001_101000/100925/Additional_files/Fig.5.tif
Fig.6.tif - Figure showing GO and KEGG enrichment analysis of DEGs. The items with noticeable differences between C. argus and C. maculata were selected for display. - https://s3.ap-northeast-1.wasabisys.com/gigadb-datasets/live/pub/10.5524/100001_101000/100925/Additional_files/Fig.6.tif
Additional_File_1.tif - Figure showing K-mer distribution of reads of C. argus (A) and C. maculata (B). - https://s3.ap-northeast-1.wasabisys.com/gigadb-datasets/live/pub/10.5524/100001_101000/100925/Additional_files/Additional_File_1.tif
Additional_File_9.tif - Figure showing GO enrichment analysis of genes in expansion/contraction families. - https://s3.ap-northeast-1.wasabisys.com/gigadb-datasets/live/pub/10.5524/100001_101000/100925/Additional_files/Additional_File_9.tif
Additional_File_12.tif - Figure showing preliminary analysis of sequencing data of C. argus. - https://s3.ap-northeast-1.wasabisys.com/gigadb-datasets/live/pub/10.5524/100001_101000/100925/Additional_files/Additional_File_12.tif
Additional_File_13.tif - Figure showing preliminary analysis of sequencing data of C. maculata. - https://s3.ap-northeast-1.wasabisys.com/gigadb-datasets/live/pub/10.5524/100001_101000/100925/Additional_files/Additional_File_13.tif
FigTree.tre - Phylogenetic tree file - https://s3.ap-northeast-1.wasabisys.com/gigadb-datasets/live/pub/10.5524/100001_101000/100925/FigTree.tre
super_gene.final.pep.phy - Phylogenetic tree associated alignment file (Phylip format) of peptides - https://s3.ap-northeast-1.wasabisys.com/gigadb-datasets/live/pub/10.5524/100001_101000/100925/super_gene.final.pep.phy
super_gene.final.nt.phy - Phylogenetic tree associated alignment file (Phylip format) of nucleotides - https://s3.ap-northeast-1.wasabisys.com/gigadb-datasets/live/pub/10.5524/100001_101000/100925/super_gene.final.nt.phy

[License]
All files and data are distributed under the CC0 1.0 Universal (CC0 1.0) Public Domain Dedication (https://creativecommons.org/publicdomain/zero/1.0/), unless specifically stated otherwise, see http://gigadb.org/site/term for more details.

[Comments]

[End]

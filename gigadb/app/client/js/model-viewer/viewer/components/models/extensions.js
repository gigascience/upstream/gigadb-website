const STL = "stl";
const OBJ = "obj";
const PLY = "ply";
const LAS = "las";

export { STL, OBJ, PLY, LAS };
